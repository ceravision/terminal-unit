//	**************************************************************************************
//	
//	Project Name: Alvara� Terminal Unit
//	
//	File Name: TRML_main.c
//	
//	Release:	C10 - FW02 - 2.1.0
//
//	
//	Author:	F. Agnello, F.Anastasi
//	
//	**************************************************************************************
//	
//	� Ceravision Ltd - 2014
//	
//	*******************************************
//	Minor changes compared to the previous version [2.0.0]
//	--------------------------------------------------------------------------
//	1. The baud rate of the UART module is changed to 115200bps (was 9600bps)
//	and the radio modem is programmed to work at the same baud rate by 
//	writing 1 in  the internal register S5
//
//	2. When booting up CmdMode_TimeOut_Timer is used to delay the time
//	when the radio modem can be programmed. Meanwhile the device can be set
//	in command mode. If FUNCTION_CODE = SET_COMMAND_MODE  the
//	device enters in command mode, Cmd_Mode_Sw is set to TRUE,
//	and remains in command mode until it will be rebooted
//	If still in normal mode when the CmdMode_TimeOut_Timer ticks
//	the application will try to programm the radio modem's registers S2 & S5
//	according to the value of Modem_Status 
//
//	3. WriteModemChannel is replaced by WriteModemRegister which
//	allows any register of the radio modem to be written
//
//	Major changes compaired to previous version [ 1.3.x ]
//	--------------------------------------------------------------------------
//	1. Added the option to reset the device by software.
// 	The function code RESET_DEVICE will reset the device.
//
//	2. Bootloading a new program code is now allowed by using the 
//	reset command described above. Bootloading is also possible at the
//	power on reset.
//
//	3. Added new error codes to make troubleshooting easier:
//
//	- FC_NACK = function code not recognize
//	- FC_CAST_ERROR = error in casting message
//	- RM_NACK_ERROR = Radio Modem messaging not acknowledge
//	- RM_TIMEOUT_ERROR = RadioModem messaging time out
//
//	4. The program code has been optimize in order to be allocatable in the
//	program memory along whit the bootloader code. The execution of the read/write
//	function code became a function returning one of the following code:

//	-  NO_ERROR
//	- CHECKSUM_ERROR
//	- MD_NACK_ERROR
//	- MD_TIMEOUT_ERROR
//	- FC_NACK
//	- FC_CAST_ERROR
//	- RM_NACK_ERROR
//	- RM_TIMEOUT_ERROR
//		
//	5. The function code Get_Unid_ID have been removed because
//	meaningless.
//
//	6. At the power on reset the channel of the radio modem device 
//	is set accordingly to what is written in the eeprom memory
//	at EEPROM_RF_CHANNEL
//
//	7. new fucntion code have been added:
//
//	- READ_RADIO_MODEM_CHANNEL_EEPROM
//	- SET_RADIO_MODEM_CHANNEL_EEPROM
//	
//	They allow for storing data about the working channel
//	into the device's eproom. When a new value is set into
//	the eeprom it will beciome active only at the next bootstrap
//	whihc means that by sending the reset command the new
//	value will be transferred into the radio modem device.
//


//	INCLUDE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <string.h>
#include<pic.h>
#include<htc.h>
#include"TRML_cnst.h"
#include"TRML_vrbl.h"


//	Firmware release
const char *FirmwareID = "C10FW02210";

//	CONFIGURATION BITS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Program config. word 1

// watchdog enabled
__CONFIG(FOSC_INTOSC & WDTE_ON & PWRTE_OFF 
			& MCLRE_ON & CP_ON & CPD_OFF & BOREN_OFF 
				& CLKOUTEN_OFF & IESO_ON & FCMEN_ON);

// Program config. word 2
//__CONFIG(WRT_OFF & PLLEN_ON & STVREN_ON
//			& BORV_19 & LVP_OFF);
__CONFIG(WRT_OFF & PLLEN_ON & STVREN_ON
			& BORV_LO & LVP_OFF);


//	FUNCTION PROTOTYPE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void HW_Unit_Config(void);
void SW_Unit_Config(void);


void WRITE_EEPROM_BYTE(unsigned char address, unsigned char data);

void GetUnitStatus(void);
void SendMessage(unsigned char Error_Code);

unsigned char Execute_FC(unsigned char Function_Code, unsigned char Dummy_ByteCount, unsigned char Inbound_Broadcast );

unsigned short getCRC16(unsigned char *s, unsigned char lenght);
unsigned char READ_EEPROM_BYTE(unsigned char address);
unsigned char ReadModemChannel(unsigned char *pData);

unsigned char WriteModemRegister(unsigned char RegAdd,unsigned char RegVal);

unsigned char VerifyIncomingMessage(unsigned char *pBuffer, unsigned char DataLength);
unsigned char Send_AT_Command( const unsigned char *c);



//	INTERRUPT SERVICE ROUTINE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void interrupt noname(void) @ 0x10
{   
	static unsigned char isr_dummy;
	unsigned char 	isr_dummy1;
	unsigned short	isr_dummy2;  

	if(RCIE==1 && PIR1bits.RCIF==1)
	{ 
		PIR1bits.RCIF = 0;
		RCIE=0;  
	
		// check if Rx error
		if(RCSTAbits.OERR==1)
		{
			RCSTAbits.CREN=0;
			RCSTAbits.CREN=1;
		}
		else if (RCSTAbits.FERR==1)
		{
			isr_dummy = RCREG;  
		}
		else // if no error then save the received byte
		{
			ExtBuffer[ExtBufferPos]=RCREG;	// save data in the buffer
			
			if (++ExtBufferPos==EXT_BUFFER_DIM) // update buffer pointer
				ExtBufferPos=0;	
			
			RxIdleCheck=1; // enable idle check timer
		}

		RCIE=1;

	}// end Receiver_ISR


//
// TMR0_ISR (every 1 msec @ 4MHz)
//
	if (TMR0IE==1 && INTCONbits.TMR0IF==1)
	{  	
		// Update Counters
		TimeOut_Timer-- ;
		PowerOnReset_Sinc_Timer--;
		CmdMode_TimeOut_Timer++;
			
		// Update inbound message flag status 
		if (!TimeOut_Timer)
			RxModemTimeOut = TRUE;
		
		// re enable interrupt on change
		if ((!PowerOnReset_Sinc_Timer) && (PowerOnReset)){
			
			PowerOnReset = FALSE;
			// clear mismatch condition if any
			portb_latch.val = PORTB;
			// reset unit status
			DryContactStatus.val=0;
			IOCIE = TRUE;
		}
		
		
		// Raise CmdMode_TimeOut flag
		if(CmdMode_TimeOut_Timer > CMD_MODE_TIMEOUT){
			
			// Signal that the radio modem must be programmed
			Check_Modem= TRUE;
			CmdMode_TimeOut_Timer  = 0;
		}
	
		// Update the master offline timer counter
		if (!(--Master_Inner_TimeOut_Counter)){
		
			Master_Inner_TimeOut_Counter = MASTER_TIMEOUT_RELOAD;
			if (! (-- Master_Outer_TimeOut_Counter ))	{
			
				// Reset timer
				Master_Outer_TimeOut_Counter  = Master_OffLine_TimeOut_Reload;        
				// set the master offline conditon
				Master_OnLine = false;
			} 
		
		}// end Update timer related flag status
			
		// INCOMING MESSAGE TIME-OUT (RX LINE IDLE STATE)
		//
		// RxIdleCheck is set to 1 each time a new incoming byte is expected.
		// When RxIdleCheck becomes higher than RX_TIMEOUT
		// the last byte in a message has been received and
		// a new message is ready to be read.
		//
		if(( RxIdleCheck) && (++RxIdleCheck>RX_IDLE_TIMEOUT))
		{
			// disable idle check timer
			RxIdleCheck=0;
		
			if(ExtBufferPos)
			{ // the buffer is full
						
				InBoundModemMessage=TRUE;
				FrameLength=ExtBufferPos;
				// reset buffer pointer
				ExtBufferPos=0;
			}
					
		}// end RxIdleCheck

		// clear TMR0 Interrupt Flag          
		INTCONbits.TMR0IF = 0;
		    
		// reset Timer0 counter value (1 msec @ 4MHz)
		TMR0=TMRO_START_VALUE;
		 
	} // end TMR0_ISR


//
// INTERRUPT_ON_CHANGE_ISR
//

	if (IOCIE && IOCIF)
	{
	
		 // a  delay is applied for debouncing
		isr_dummy2 = 0x3E8;
		while (-- isr_dummy2) {asm("nop");}
		
		// clear the general flag bit IOCIF
		IOCIF = 0;
	
		// end the mismatch condition
		isr_dummy1 = PORTB;
		
		// look for the interrupt source
		int_on_change.val = IOCBF &  0xFF;
		portb_latch.val = isr_dummy1;
	
	
		// INTERRUPT_ON_CHANGE_RB0
		if (int_on_change.line.RB0)
		{
			 asm("nop");
			 if (!portb_latch.line.RB0) 				
			 {
				   DryContactStatus.line.LN0 |= 1;
				   asm("nop");
			 }
			      
			 asm("nop");
	
			 // clear the single flag for IOCBF0
			 IOCBF0 = 0;
		}
	
	
		if (int_on_change.line.RB1){asm("nop");}
		
		if (int_on_change.line.RB2){asm("nop");}
	
	
		// INTERRUPT_ON_CHANGE_RB3
		if (int_on_change.line.RB3)
		{
		
			asm("nop");
			if (!portb_latch.line.RB3) 				
			{
				DryContactStatus.line.LN3 |= 1;
		              	asm("nop");
			}
		
			asm("nop");
	
			// clear the single flag for IOCBF3
			IOCBF3 = 0;
		}
	
	
		// INTERRUPT_ON_CHANGE_RB4
		if (int_on_change.line.RB4)
		{
	
			asm("nop");
			if (!portb_latch.line.RB4)
			{
				DryContactStatus.line.LN4 |= 1;
				asm("nop");
			}
	
			asm("nop");	
	
			// clear the single flag for IOCBF4
			IOCBF4 = 0;
		
		}
	
	
		// INTERRUPT_ON_CHANGE_RB5
		if (int_on_change.line.RB5)
		{
	
			asm("nop");
			if (!portb_latch.line.RB5) 				
			{
	
				DryContactStatus.line.LN5 |= 1;
				asm("nop");
	
			}
	
			asm("nop");	
		
			// clear the single flag for IOCBF5
			IOCBF5 = 0;
	
		} // end Push Button ON
	
	
	
		// INTERRUPT_ON_CHANGE_RB6
		if (int_on_change.line.RB6)
		{
			asm("nop");
//	
//			if (!portb_latch.line.RB6) 				
//			{
//				DryContactStatus.line.LN6 |= 1;
//				asm("nop");
//			}
//		
//			asm("nop");
//	
//			// clear the single flag for IOCBF6
//			IOCBF6 = 0;		
		
		}
	
	
		// INTERRUPT_ON_CHANGE_RB7
		if (int_on_change.line.RB7)
		{
			asm("nop");
//			if (!portb_latch.line.RB7) 				
//			{
//				DryContactStatus.line.LN7 |= 1;
//				asm("nop");
//			}
//		
//			asm("nop");
//	
//			// clear the single flag for IOCBF7
//			IOCBF7 = 0;

		}
	
	
	}// end INTERRUPT_ON_CHANGE_ISR


}//  end interrupt service routine code



//	FUNCTION DEFINITION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**************************************************************************
 *  
 *  Send_AT_Command
 *  
 * This function sends an AT command to the radio modem device. The string is
 * given by the caller as the pointer to the string, and the fucntion returns one of the
 * folowings codes:
 *
 *   Returned value:
 *	- NO_ERROR
 *	- RM_TIMEOUT_ERROR
 *
 ***************************************************************************/
unsigned char Send_AT_Command(const unsigned char  *String){ 

const unsigned char  *s1;

	s1 = String;
	
	while (*(String)!='\0') 
	{ 
		TXREG = *(String);
	 	while (!TXSTAbits.TRMT);
		String ++;
	} 

	if (*s1 != '+')
	{
		TXREG = 0x0D; //Carriage return 
		while (!TXSTAbits.TRMT); 
		TXREG = 0x0A; //Line Feed 
		while (!TXSTAbits.TRMT);
	}

	// Set up time out timer
	TimeOut_Timer = RM_OUTBOUND_TIMEOUT;
   	InBoundModemMessage = FALSE;
   	RxModemTimeOut = FALSE;

   	while (!(InBoundModemMessage||RxModemTimeOut));

	// Exit when time out
	if (RxModemTimeOut){
		return RM_TIMEOUT_ERROR;
	}
	else{
		return NO_ERROR;
	}
	
}// end of Send_AT_Command

/**************************************************************************
 *  
 *  ReadModemChannel
 *  
 * This function interacts with the radio modem device by asking what's the contents of S2 register
 * which holds the value of the working channel. If no errors occur during the process, the valid
 *  information will be available in ExtBuffer[0].
 *
 * Send_AT_Command returns no_error if somenthing is received back from
 * the modem but the meaning of the answer must be evaluated from the calling
 * function. Otherwise, the time out error code is returned.
 *
 *   Returned value:
 *	- NO ERROR
 *	- RM_NACK_ERROR
 *	- RM_TIMEOUT_ERROR
 *
 ***************************************************************************/
unsigned char ReadModemChannel(unsigned char *pData){

	unsigned char RM_Comm_Error;
//	unsigned char index;
	
	RM_Comm_Error = Send_AT_Command("+++");
	
	if (!(RM_Comm_Error)){
		
		// check the answer if not "OK"
		if (strncmp(&ExtBuffer[0],"OK",2)){
			RM_Comm_Error = RM_NACK_ERROR;
		}
		else{
			RM_Comm_Error = Send_AT_Command("ATS2");
			if (!(RM_Comm_Error)){
				RM_Comm_Error = RM_NACK_ERROR;
				// check the answer if Ch = [0 .. 9]
				if((0x30 <= ExtBuffer[0]) && (ExtBuffer[0] <= 0x39)){
					(*pData) = ExtBuffer[0] -0x30;
					RM_Comm_Error = NO_ERROR;
				}	
			}
		}
			
		// Set the modem in data mode before return
		Send_AT_Command("ATCC");
	}
	
	return RM_Comm_Error;

}// end of ReadModemChannel


/**************************************************************************
 *  
 *  WriteModemRegister Function
 *  
 * This function writes RegVal into RegAdd register
 *
 * Send_AT_Command returns no_error if somenthing is received back from
 * the modem but the meaning of the answer must be evaluated from the calling
 * function. Otherwise, the time out error code is returned.
 *
 *   Returned value:
 *	- NO ERROR
 *	- RM_NACK_ERROR
 *	- RM_TIME_OUT
 *
 ***************************************************************************/
unsigned char WriteModemRegister(unsigned char RegAdd,unsigned char RegVal)
{

	unsigned char RM_Comm_Error;
	unsigned char WriteChannel[10];
	
	WriteChannel[0]='A';
	WriteChannel[1]='T';
	WriteChannel[2]='S';
	WriteChannel[3]= RegAdd + 0x30;
	WriteChannel[4]='=';
	WriteChannel[5]= RegVal + 0x30;
	WriteChannel[6]= ',';
	WriteChannel[7]= 'W';
	WriteChannel[8]= 'R';
	WriteChannel[9]='\0';
	
	RM_Comm_Error = Send_AT_Command("+++");
	if (!(RM_Comm_Error)){
		
		// check the answer if not "OK"
		if (strncmp(&ExtBuffer[0],"OK",2)){
			RM_Comm_Error = RM_NACK_ERROR;
		}
		else{
			RM_Comm_Error = Send_AT_Command(WriteChannel);
			if (!(RM_Comm_Error)){
				// check the answer if not "OK"
				if(strncmp(&ExtBuffer[0],"OK",2)){
					RM_Comm_Error = RM_NACK_ERROR;			
				}
			}
		}
		// Set the modem in data mode before return
		Send_AT_Command("ATCC");
	}
	
	return RM_Comm_Error;

}// end of WriteModemRegister


/**************************************************************************
 *  
 *  Verify_Incoming_Message
 *  
 * This function checks if the inbound message is addressed to the unit either as a unicast, 
 * multicast or a broadcast message
 *
 ***************************************************************************/
unsigned char Verify_Incoming_Message(unsigned char *pBuffer, unsigned char DataLength){
	
	unsigned short CheckReceivedCRC;
	unsigned char Comm_Error;
	

	Comm_Error = NO_ERROR;
			
	CheckReceivedCRC = (*(pBuffer + DataLength - 2) <<8)|(*(pBuffer + DataLength - 1));
	CheckReceivedCRC -= getCRC16(pBuffer,DataLength-2);
	
	if (CheckReceivedCRC){
		Comm_Error = CHECKSUM_ERROR;
	}
	// Check if Broadcast	
	else if ((ExtBuffer[GROUP_ADDRESS] != UnitID.GroupAddress) && (ExtBuffer[GROUP_ADDRESS]  != BROADCAST)){
		//Group Address Error
		Comm_Error = NACK_ERROR;
	}
	// check if multicast
	else if ((ExtBuffer[UNIT_ADDRESS] != UnitID.UnitAddress) && (ExtBuffer[UNIT_ADDRESS] != MULTICAST)){
		// Unit Address Error
		Comm_Error = NACK_ERROR;
	 }
	
	if (Comm_Error != CHECKSUM_ERROR){
		
		// Reset master off-line timer
		Master_Inner_TimeOut_Counter = MASTER_TIMEOUT_RELOAD;
		Master_Outer_TimeOut_Counter = Master_OffLine_TimeOut_Reload;
		
		// set the master on line condition
		Master_OnLine = TRUE;
	}
		 		
	return Comm_Error;

}// end of VeirfyIncomingMessage



/**************************************************************************
 *  
 *   getCRC16
 *  
 ***************************************************************************/
unsigned short getCRC16(unsigned char *s, unsigned char lenght){

	 unsigned short CRCRegFull;
	 unsigned short index=0;
	 unsigned char x=0;
	 unsigned char temp=0;
	 unsigned char CheckLSB=0;
	
	 /* load the register with 0xFFFF*/
	 CRCRegFull= 0xFFFF;
	
	 for(index=0; index<lenght; index++) {
		  temp=s[index];
		
		  /* XOR Low-Byte order of CRCReg and s */
		  CRCRegFull^=(unsigned short)(temp);
		
		  for (x=0; x<8; x++){
			  
			  CheckLSB=( (0x01) & (unsigned char)(CRCRegFull)) ;  
		    	/* Bit-shift once to the right */
		   	CRCRegFull = CRCRegFull>>1;
		   	/* Evaluate LSB. If it==1, XOR with 0xA001 */
		   	if (1 & CheckLSB){
			   	CRCRegFull^=0xA001;
		   	 }
		   	 
		  }/* Repeat for each bit in the byte */
	
	}/* Repeat for each character in the sequence */
	
	return((CRCRegFull & 0x00FF)<<8) | ((CRCRegFull & 0xFF00)>>8);
 
} // end of getCRC16
 

/**************************************************************************
 *  
 *  READ_EEPROM_BYTE
 *  
 ***************************************************************************/
unsigned char READ_EEPROM_BYTE(unsigned char address){

// To read a data memory location, the user must write the
// address to the EEADRL register, clear the EEPGD and
// CFGS control bits of the EECON1 register, and then
// set control bit RD. The data is available at the very next
// cycle, in the EEDATL register; therefore, it can be read
// in the next instruction. EEDATL will hold this value until
// another read or until it is written to by the user (during
// a write operation).

// 	// wait until (possible) previous write-acces is finished
//	while(EECON1bits.WR == 1);
//   	// write enable bit clear
//	EECON1bits.WREN = 0;

	// Data Memory Address to read
	EEADR = address;

	// Deselect Config space
	EECON1bits.CFGS = 0;

	// Point to DATA memory
	EECON1bits.EEPGD = 0;
	
	// EE Read
	EECON1bits.RD = 1;

	return EEDATA;

}


/**************************************************************************
 *  
 *  WRITE_EEPROM_BYTE
 *  
 ***************************************************************************/
void WRITE_EEPROM_BYTE(unsigned char address, unsigned char data){
	 
// The user must follow a specific sequence to initiate 
// the write for each byte.

// The write will not initiate if the following sequence is not
// followed exactly: write 55h to EECON2, write AAh to
// EECON2, then set WR bit, for each byte. Interrupts
// should be disabled during this code segment.

// Additionally, the WREN bit in EECON1 must be set to
// enable write. This mechanism prevents accidental
// writes to data EEPROM due to errant (unexpected)
// code execution (i.e., lost programs). The user should
// keep the WREN bit clear at all times, except when
// updating EEPROM. The WREN bit is not cleared
// by hardware.
// After a write sequence has been initiated, clearing the
// WREN bit will not affect this write cycle. 

// The WR bit will be inhibited from being set unless
// the WREN bit is set.
// At the completion of the write cycle, the WR bit is
// cleared in hardware and the EE Write Complete
// Interrupt Flag bit (EEIF) is set. The user can either
// enable this interrupt or poll this bit. EEIF must be
// cleared by software

	// Data Memory Address to write
	EEADR = address;
	// Data Memory Value to write
	EEDATA = data;

	// Deselect Configuration space
	EECON1bits.CFGS = 0;

	// Point to DATA memory
    	EECON1bits.EEPGD = 0;
  
	// Enable writes
    	EECON1bits.WREN = 1;

	// Disable INTs
    	INTCONbits.GIE = 0;

// required sequence
//
    	EECON2 = 0x55;
   	EECON2 = 0xAA;
    	EECON1bits.WR = 1;   // start writing
//
//end required sequence

	// Disable writes
	EECON1bits.WREN = 0;

	// Wait for write to complete
	while(!EEIF)
	{
		asm("nop");
		// Disable writes
		EECON1bits.WREN = 0;
	}

	// EEIF must be cleared by software
	EEIF = 0;

	// Disable writes
    	EECON1bits.WREN = 0;

	// enable INTs
	INTCONbits.GIE = 1;

}
 

/**************************************************************************
 *  
 *  SendMessage Procedure
 *  
 ***************************************************************************/
void SendMessage(unsigned char Error_Code){

	unsigned dummy_index;
	unsigned char Dummy_ByteCount;
	unsigned short CRC16;
	
	if (Error_Code)
	{
		// Mapping outbound message
		OutModemBuffer[FUNCTION_CODE] = FUNCTION_CODE_ERROR;
		OutModemBuffer[BYTE_COUNT] = 1;
		OutModemBuffer[BYTE_COUNT +1 ] = Error_Code;
	}
	
		Dummy_ByteCount = OutModemBuffer[BYTE_COUNT] + FRAME_HEADER_LEN;
	
		// Add CRC16
		CRC16=getCRC16(OutModemBuffer,Dummy_ByteCount);
		OutModemBuffer[Dummy_ByteCount]=(CRC16 & 0xFF00)>>8;
		Dummy_ByteCount ++;
		OutModemBuffer[Dummy_ByteCount]=(CRC16 & 0x00FF);
		Dummy_ByteCount ++;
						       
		// Transmit
		for (dummy_index=0; dummy_index<Dummy_ByteCount;dummy_index++)
		{
			TXREG=OutModemBuffer[dummy_index]; 
			while (!TXSTAbits.TRMT);
		}

} // end SendMessage


 /**************************************************************************
 *  
 *   HW_Unit_Config Procedure
 *  
 ***************************************************************************/
void  HW_Unit_Config(void){
	
	 TRISA = 0x00; 
	 TRISB = 0b00111011; //RB1 RX     RB0,RB3,RB4,RB5 Interrupts on change
	 ANSELA=0x00;
	 ANSELB=0x00;
	 PORTA= 0x00;
	 PORTB= 0x00;
	
	 WPUB=0x00;
	
	 
	//Internal Clock Source Settings...
	 OSCCON=0x6A;		//4Mhz
	 
	//TIMER0 Settings....
	 OPTION_REG=0xC7;

	 // Interrupts Settings...
	INTCON = 0xE8;
	

	//Interrupt On Change Rising Edge      
	IOCBP=0x00;
	
	//Interrupt On Change Falling Edge      
	//IOCBN=0x39;
	IOCBN =  STATUS_MASK;
	PIE1=0x20;
	PIE2=0x00;
	PIR1=0x00;
	PIR2=0x00;
	APFCON0=0x00;
	APFCON1=0x00;
	
	// Serial Module Settings
	TXSTA=0x20;
	RCSTA=0x90;
	
	SYNC = 0; 	
	BRGH = 1; 
	BRG16 = 1; 
	SPBRG=8;	//115kbps @ 4Mhz 

} // end of Unit_HW_Config


 /**************************************************************************
 *  
 *  Unit_Config of the software profile
 *  
 ***************************************************************************/
void SW_Unit_Config(void){
	
	unsigned char Dummy_Byte;
//	unsigned short Comm_Error;
	
// clear mismatch condition if any
	portb_latch.val = PORTB;
	
// reset unit status
	DryContactStatus.val=0;
	
//
// Power Up delay
//
//	PowerOnReset_Sinc_Timer =  POWER_ON_RESET_TIMEOUT;
//	while (PowerOnReset_Sinc_Timer){	CLRWDT();}
//	PowerOnReset = TRUE;
	
	// disable interuupt on change when (power on)/(power on reset)
	// it will re enabled when PowerOnReset_Sinc_Timer elapses
	IOCIE = FALSE;
	PowerOnReset_Sinc_Timer =  POWER_ON_RESET_TIMEOUT;
//	while (PowerOnReset_Sinc_Timer){	CLRWDT();}
	PowerOnReset = TRUE;

	
//
// System config
//
	UnitID.GroupAddress=READ_EEPROM_BYTE(EEPROM_GROUP_ADD);
	UnitID.UnitAddress=READ_EEPROM_BYTE(EEPROM_UNIT_ADD);
	
//
// RESET THE MASTER OFFLINE TIMERS
//
// Master On Line timeout is measured by using two nested loops. The inner loop uses
// the Master_Inner_TimeOut_Counter timer, while the outer uses the
// Master_Outer_TimeOut_Counter timer. The inner timer always uses the code fixed value
// MASTER_TIMEOUT_RELOAD, while the outer can be use values that are assigned by the user writing the
// desired value in EEPROM_MASTER_TIMEOUT. At the reset if EEPROM_MASTER_TIMEOUT is empty
// then the software will assign the default value given by MIN_MASTER_TIMEOUT
	
		// Set Inner timer
		Master_Inner_TimeOut_Counter = MASTER_TIMEOUT_RELOAD;
		
		// Set Outer timer
		if ((Master_OffLine_TimeOut_Reload = READ_EEPROM_BYTE(EEPROM_MASTER_TIMEOUT)) == 0xFF){
			Master_OffLine_TimeOut_Reload = MIN_MASTER_TIMEOUT;
			WRITE_EEPROM_BYTE(EEPROM_MASTER_TIMEOUT,MIN_MASTER_TIMEOUT);
		}
		Master_Outer_TimeOut_Counter = Master_OffLine_TimeOut_Reload;
		
//
// SET MODEM OPERATING MODES
//
// The value of the working channel is stored in the register S2 of the radio modem device and can
// be changed by the user. The expected value is also stored in the eeprom (EEPROM_RF_CHANNEL)
// and used as reference when bootingup: if EEPROM_RF_CHANNEL is not blank then its value is 
// copied into S2.
//
// New @2.1.0
//
// The value of the operating baudrate is stored in the register S5 of the radio modem device.
// The expected value for the baurate is hard coded and set when booting up
//
// Check_Modem is a flag signalling when the radio modem can be programmed. The flag is used to delay
// the time when the modem will be programmed compared to boot time.
// Modem_Status is a variable stating which register of the radio modem needs to be programmed.
// 

		// All the registers need to be programmed
		Modem_Status.val = 1;
		// Delay the radio modem program time
		Check_Modem= FALSE;
		// Reset the timeout timer counter
		CmdMode_TimeOut_Timer = 0;

//
// SET COMMAND MODE WAITING STATUS
//		
		// Reset command mode flag
		Cmd_Mode_Sw = FALSE;
		
		
//
// GENERAL SETTINGS
	
		// Set default online master status = False
		Master_OnLine = false;
	
		// Set the initial status: IDLE
		SystemStatus=IDLE;
	
} // end of Unit_SW_Config	
 
  /**************************************************************************
 *  
 *  Execute FC
 *  
 *	Execute the fucntion code given as Fucntion_Code and return:
 *  
 *	-  NO_ERROR
 *	- CHECKSUM_ERROR
 *	- MD_NACK_ERROR
 *	- MD_TIMEOUT_ERROR
 *	- FC_NACK
 *	- FC_CAST_ERROR
 *	- RM_NACK_ERROR
 *	- RM_TIMEOUT_ERROR
 *
 *	Inbound_Message is used as a flag for skipping execution of
 *	codes not allowed if the messages are broadcast/multicast
 *	When no errors occur OutModemBuffer will contain the message
 *	to be send back to the master.
 ***************************************************************************/
unsigned char Execute_FC(unsigned char Function_Code, unsigned char Dummy_ByteCount, unsigned char Inbound_Broadcast ){
	
	unsigned short index;
	unsigned char Dummy_DataPos;
	unsigned char Dummy_Byte;
//	unsigned char Comm_Error;
	unsigned char *Dummy_P1;
	unsigned char *Dummy_P2;
	
	unsigned char FC_Comm_Error;
		
	switch (Function_Code){
			
			
// Not Acknwoledged READ codes
//
//	READ_RAM
//	READ_ALL_EEPROM
//	READ_EEPROM
//	GET_SCENE - FC
//	READ_ALL_EEPROM
//

//
// READ_SLAVE_STATUS - FC: 0x05 (5)
//
		case READ_SLAVE_STATUS:
			
			if (!(Inbound_Broadcast)){
				
				// Mapping outbound message:
				// data pointer adjustment
				Dummy_ByteCount ++;

				// Fill up Data Field

////////////////////////// Dry Contact Status

				OutModemBuffer[Dummy_ByteCount] = DryContactStatus.val & STATUS_MASK;
				Dummy_ByteCount ++;

////////////////////////// PortB Contact Status

				// portb_latch.val = PORTB;
				OutModemBuffer[Dummy_ByteCount] = PORTB & STATUS_MASK;
				Dummy_ByteCount ++;

				// Outbound data byte count
				OutModemBuffer[BYTE_COUNT]= Dummy_ByteCount - FRAME_HEADER_LEN;

			  	FC_Comm_Error = NO_ERROR;
			  	if (PowerOnReset){
				  	FC_Comm_Error = FC_NACK;
				 }	
			  	
			}// end of else  	
		
			break; //  End Function Code: READ_SLAVE_STATUS

	
//
// READ_RADIO_MODEM_CHANNEL - FC: 0x06(6)
//
	
		case READ_RADIO_MODEM_CHANNEL:
		
			// Exit if the message is received while in Command Mode and after having sent out an error message
			if(Cmd_Mode_Sw){
		  		FC_Comm_Error = FC_NACK;
			}		
			else if(!(Inbound_Broadcast)){
				
				// Mapping outgoing message
				//  outgoing data byte count
				OutModemBuffer[Dummy_ByteCount ] = 1;
				Dummy_ByteCount ++;
				FC_Comm_Error = ReadModemChannel(&OutModemBuffer[Dummy_ByteCount]);
					
			} // end if (!(Inbound_Broadcast))
	
			break; // End Function Code: READ_RADIO_MODEM_CHANNEL
	
//
// GET_DEVICE_ID - FC: 0x0A (10)
//
	
		case GET_DEVICE_ID:
	
			if (!(Inbound_Broadcast)){
						
				// Mapping outbound message
				
				//  Outbound data byte count
				DataLength = READ_EEPROM_BYTE(EEPROM_DEVICE_ID);
		
				// exit when the empty value(255) is read
				if (DataLength == VALUE_EMPTY){DataLength=0;}
		
				OutModemBuffer[BYTE_COUNT]= DataLength;
				Dummy_ByteCount ++;
	
				EEProm_Address = EEPROM_DEVICE_ID + 1;					
				// Data Field
				for (index=0;index<DataLength; index ++)
				{
					OutModemBuffer[Dummy_ByteCount] = READ_EEPROM_BYTE(EEProm_Address);
					Dummy_ByteCount ++;
					EEProm_Address ++;
				}
	
			  	FC_Comm_Error = NO_ERROR;
			  	
			} // end if (!(Inbound_Broadcast))
			
			break; // End Function Code: GET_DEVICE_ID
	
//
// GET_FW_ID - FC: 0x0B (11)
//
	
		case GET_FW_ID:
		
			if (!(Inbound_Broadcast)){
				
				// Mapping outbound message
		
				OutModemBuffer[BYTE_COUNT] = 0;
				Dummy_ByteCount ++;
	
				// Data Field
				Dummy_P1 = (unsigned char *) FirmwareID;
				while (*(Dummy_P1) != '\0') 
				{ 
					OutModemBuffer[Dummy_ByteCount] = *(Dummy_P1);
					Dummy_ByteCount ++;
					Dummy_P1 ++ ;
				} 
				
				//  outbound data byte count
				OutModemBuffer[BYTE_COUNT] = Dummy_ByteCount -  FRAME_HEADER_LEN;
	
			  	FC_Comm_Error = NO_ERROR;
			  	
			} // end if (!(Inbound_Broadcast)) 	

			break; // End Function Code: GET_FW_ID
	
//
// GET_MASTER_TIMEOUT - FC: 0x0E (14)
//

		case GET_MASTER_TIMEOUT: case READ_RADIO_MODEM_CHANNEL_EEPROM:
		
			if (!(Inbound_Broadcast)){
				
				// Mapping outbound message
						
				// data pointer adjustment
				Dummy_ByteCount ++;
				
				// Data Field
				EEProm_Address = EEPROM_MASTER_TIMEOUT;
				if (Function_Code == READ_RADIO_MODEM_CHANNEL_EEPROM){
					EEProm_Address = EEPROM_RF_CHANNEL;
				}	
				OutModemBuffer[Dummy_ByteCount] = READ_EEPROM_BYTE(EEProm_Address);
				Dummy_ByteCount ++;
	
				//  outbound data byte count
				OutModemBuffer[BYTE_COUNT]= Dummy_ByteCount - FRAME_HEADER_LEN;
	
			  	//SendMessage(NO_ERROR);
			  	FC_Comm_Error = NO_ERROR;
			  	
			}  // end if (!(Inbound_Broadcast))	
			
			break; // End Function Code: GET_MASTER_TIMEOUT
	

//
// WRITE_RAM or WRITE_EEPROM - FC: 0x80 (128) or 0xC0 (192)
//

// Not Acknwoledged WRITE codes
//
//	WRITE_EEPROM
//	GOTO_SCENE
//	SET SCENE


//
// WRITE_RAM - FC: 0x80 (128)
//

		case WRITE_RAM:

			if (!(Inbound_Broadcast)){
		
		
				// Mapping outgoing message
				
				//  outgoing data byte count
				DataLength = ExtBuffer[BYTE_COUNT];
	
				OutModemBuffer[Dummy_ByteCount] = DataLength >> 1;
				Dummy_ByteCount ++;
	
	//////////////////////////////////////////////////////////
	
				// Data Field			
				Dummy_DataPos =  Dummy_ByteCount;
				while (Dummy_DataPos < (DataLength + FRAME_HEADER_LEN))
				{ 
				
					Dummy_P1 = 0x00;
					Dummy_Byte = ExtBuffer[Dummy_DataPos] - RAM_POSITIONING_OFFSET;
					Dummy_DataPos ++ ;
	
	
					*(Dummy_P1 + Dummy_Byte) = ExtBuffer[Dummy_DataPos];
					Dummy_DataPos ++ ;
	
					OutModemBuffer[Dummy_ByteCount] = *(Dummy_P1 + Dummy_Byte);
					Dummy_ByteCount ++ ;
					
				} // end while loop
	
			  	//SendMessage(NO_ERROR);
			  	FC_Comm_Error = NO_ERROR;
			  	
			} // end case Function Code: WRITE_RAM
			break;

//
// SET_RADIO_MODEM_CHANNEL - FC: 0x86(134)
//

	case SET_RADIO_MODEM_CHANNEL:
	
		// Exit if the message is received while in Command Mode and after having sent out an error message
		if(Cmd_Mode_Sw){
	  		FC_Comm_Error = FC_NACK;
		}		
		else if(!(Inbound_Broadcast)){
			
			// Mapping outbound message
			
			// Outbound data byte count
			OutModemBuffer[Dummy_ByteCount] = 1;
			Dummy_ByteCount ++;
			
			OutModemBuffer[Dummy_ByteCount] = ExtBuffer[DATA_START];
			FC_Comm_Error = WriteModemRegister(AUREL_CHANNEL_REGISTER,ExtBuffer[DATA_START]);
			if (!(FC_Comm_Error)){
				// Save data in EEPROM
				WRITE_EEPROM_BYTE(EEPROM_RF_CHANNEL,ExtBuffer[DATA_START]);
			}
			
		} // end if(BroadCast)

		break; // End Function Code: READ_RADIO_MODEM_CHANNEL
		
//
// SET_RADIO_MODEM_CHANNEL_EEPROM - FC: 0x87(135)
//
	case SET_RADIO_MODEM_CHANNEL_EEPROM:
	
		// Mapping outbound message
		
		//  outbound data byte count
		OutModemBuffer[Dummy_ByteCount]= 1;
		Dummy_ByteCount ++;
		
		EEProm_Value= ExtBuffer[BYTE_COUNT+1];

		// check time out boundaries
		FC_Comm_Error = FC_CAST_ERROR;
		if ( EEProm_Value < 0x0A){
			
			EEProm_Address =  EEPROM_RF_CHANNEL;
			WRITE_EEPROM_BYTE(EEProm_Address, EEProm_Value);
			OutModemBuffer[Dummy_ByteCount] = READ_EEPROM_BYTE(EEProm_Address);
			Dummy_ByteCount ++;
			FC_Comm_Error = NO_ERROR;
		}

               	break; // End Function Code: SET_RADIO_MODEM_CHANNEL_EEPROM

//
// SET_DEVICE_ID - FC: 0x8A (138)
//

	case SET_DEVICE_ID:
	
		// Exit if the message received is broadcast/multicast. They are not supported by this function code
		if(!(Inbound_Broadcast)){

			// Mapping outbound message
								
			//  outbound data byte count
			OutModemBuffer[Dummy_ByteCount] = ExtBuffer[BYTE_COUNT];
			Dummy_ByteCount ++;
	
			if (ExtBuffer[BYTE_COUNT] > MAX_ID_LEN){
				FC_Comm_Error = FC_CAST_ERROR;
			}
			else{
				
				// eeprom modify/read
				WRITE_EEPROM_BYTE((EEPROM_DEVICE_ID),ExtBuffer[BYTE_COUNT]);
		
				// Data Field
				Dummy_P1 = &ExtBuffer[DATA_START];
				EEProm_Address = EEPROM_DEVICE_ID + 1;
				for (index=0;index<ExtBuffer[BYTE_COUNT]; index ++){
					// eeprom modify/read
					WRITE_EEPROM_BYTE((EEProm_Address + index),*(Dummy_P1));
					OutModemBuffer[Dummy_ByteCount] = READ_EEPROM_BYTE(EEProm_Address + index);
					Dummy_ByteCount ++;
					Dummy_P1 ++;
				}
		
				FC_Comm_Error = NO_ERROR;
				
			} // end if (ExtBuffer[BYTE_COUNT] > MAX_ID_LEN)
			
		} // end if(!(Inbound_Broadcast))	  	
		
		break; // End Function Code: SET_DEVICE_ID

//
// SET_UNIT_ID - FC: 0x8C (140)
//

	case SET_UNIT_ID:
					
		// Exit if the message received is broadcast/multicast. They are not supported by this function code
		if(!(Inbound_Broadcast)){

			// Exit if either the new GroupAddress or the new UnitAddress is equal to zero: not accepted
			if ((ExtBuffer[DATA_START] == 0) || (ExtBuffer[DATA_START+1]) == 0){
				FC_Comm_Error = FC_CAST_ERROR;
			}
			else{
			
				// Mapping outbound message
				
				//  outbound data byte count					
				OutModemBuffer[Dummy_ByteCount]=ExtBuffer[BYTE_COUNT];
				Dummy_ByteCount ++;
				
				// Data Field
				Dummy_P1 = &ExtBuffer[DATA_START];
				Dummy_P2 = &UnitID.GroupAddress;
				for (index=0;index<ExtBuffer[BYTE_COUNT]; index ++)
				{
					WRITE_EEPROM_BYTE((EEPROM_GROUP_ADD + index),*(Dummy_P1));
					*(Dummy_P2) = READ_EEPROM_BYTE(EEPROM_GROUP_ADD + index);
					OutModemBuffer[Dummy_ByteCount] = *(Dummy_P2);
					Dummy_ByteCount ++;
					Dummy_P1 ++;
					Dummy_P2 ++;
				}

			  	FC_Comm_Error = NO_ERROR;
			  	
			} // end (!(ExtBuffer[DATA_START] & ExtBuffer[DATA_START+1]))
			
		} // end if(!(Inbound_Broadcast))

		break; // End Function Code: SET_UNIT_ID

//
// SET_MASTER_TIMEOUT - FC: 0x8E (142)
//

	case SET_MASTER_TIMEOUT:
	
		// Mapping outbound message
		
		//  outbound data byte count
		OutModemBuffer[Dummy_ByteCount] = 1;
		Dummy_ByteCount ++;
		
		EEProm_Value = ExtBuffer[BYTE_COUNT+1];

		// check time out boundaries
		if ( EEProm_Value > MAX_MASTER_TIMEOUT ){
			EEProm_Value = MAX_MASTER_TIMEOUT;
		}
		else if ( EEProm_Value < MIN_MASTER_TIMEOUT ){
			EEProm_Value = MIN_MASTER_TIMEOUT;
		}


		EEProm_Address =  EEPROM_MASTER_TIMEOUT;
		WRITE_EEPROM_BYTE(EEProm_Address, EEProm_Value);

		Master_OffLine_TimeOut_Reload = READ_EEPROM_BYTE(EEProm_Address);
		OutModemBuffer[Dummy_ByteCount] = Master_OffLine_TimeOut_Reload;
		Dummy_ByteCount ++;

		// Reset master off-line timer
		Master_Inner_TimeOut_Counter = MASTER_TIMEOUT_RELOAD;
		Master_Outer_TimeOut_Counter = Master_OffLine_TimeOut_Reload;
		
		FC_Comm_Error = NO_ERROR;

               	break; // End Function Code: SET_MASTER_TIMEOUT

	default:
	
		FC_Comm_Error = FC_NACK;
		break; // End DEFAULT

	}// end of switch
	
	return FC_Comm_Error;
	
}// end of Execute_Write_FC
 
 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	MAIN PROGRAM
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void main(){ 

	unsigned char Dummy_ByteCount;
//	unsigned char Dummy_DataPos;
	unsigned char Dummy_Byte;
	
	unsigned char Comm_Error;
	
//	unsigned char *Dummy_P1;
//	unsigned char *Dummy_P2;
	
//
// Device HW CONFIG
	 HW_Unit_Config();
	 
//
// Device SW CONFIG
	 SW_Unit_Config();
	 	
// Big Loop
//
	while(1){  
	  
	// RESET WATCHDOG TIMER
		CLRWDT();
	
		switch(SystemStatus){
	  
	// IDLE STATE
	//
	// The system remains in the Idle state as long as:
	// 1.  A new message from the master controller is received
	//
			case IDLE:
	
				if (InBoundModemMessage){
					
	// INBOUND MESSAGE			     	
	// Check if a new valid message from the master controller has been received
	
					// Reset InBoundModemMessage flag
					InBoundModemMessage = FALSE;
			
					// Validate inbound message
					Comm_Error = Verify_Incoming_Message(&ExtBuffer,FrameLength);
					if (!(Comm_Error)){
							
						// (CRC: ACK; Group Address: ACK; Unit Address: ACK)
						// unicast, multicast or broadcast message
						SystemStatus = COMMAND_RECEIVED;
										
					} // end case (CRC: ACK; Group Address: ACK; Unit Address: ACK)
								
				} // end if InBoundModemMessage
				
				if ((!(Cmd_Mode_Sw)) && (Check_Modem)){
					
					Check_Modem = FALSE;
					
					if(Modem_Status.stat.RF_CH) {
						// Assign the default RF Channel value at booting up
						Dummy_Byte = READ_EEPROM_BYTE(EEPROM_RF_CHANNEL);
						if(Dummy_Byte < 0x0A){
							//Comm_Error = WriteModemRegister(AUREL_CHANNEL_REGISTER,Dummy_Byte);
							if(!(WriteModemRegister(AUREL_CHANNEL_REGISTER,Dummy_Byte)))
								Modem_Status.stat.RF_CH = 0;
						}
					}// end of  Modem_Status.stat.RF_CH
					if (Modem_Status.stat.BPS){
						// Assign the default bitrate value at booting up
						if(! (WriteModemRegister(AUREL_BAUDRATE_REGISTER,AUREL_HIGH_BAUDRATE)))
							Modem_Status.stat.BPS =0;
					}// end of  Modem_Status.stat.BPS
				}// end of (!(Cmd_Mode_Sw))				
			
			       	break;  // exit IDLE back to switch(SystemStatus) 
	
	
	// COMMAND_RECEIVED STATE
	//
	// the system moves from the IDLE state into the COMMAND_RECEIVED state
	// as soon as a new message from the master controller has been stored in
	// the receiver buffer
	//
	
			case COMMAND_RECEIVED:
	        
				// set the new system status @ IDLE
				SystemStatus=IDLE;
	
				// Mapping the header of the outgoing message:
				// Group Address + Unit Address + Function Code
	
				Dummy_ByteCount = 0;
	
				//  Group Addres + Unit Address
				OutModemBuffer[Dummy_ByteCount]= UnitID.GroupAddress;
				Dummy_ByteCount ++;
				OutModemBuffer[Dummy_ByteCount]= UnitID.UnitAddress;
				Dummy_ByteCount ++;
						
				// Function Code
				OutModemBuffer[Dummy_ByteCount]=ExtBuffer[FUNCTION_CODE];
				Dummy_ByteCount ++;
				
				if (ExtBuffer[FUNCTION_CODE] == SET_COMMAND_MODE){
				
					// Set_Command_Mode can be received only when wired connected to a controller
					// Set the command mode flag to TRUE and reset the timer
					
					CmdMode_TimeOut_Timer = 0;
					Cmd_Mode_Sw = TRUE;
				
					// Mapping outbound message:
	
					// Outbound data byte count
					OutModemBuffer[Dummy_ByteCount] = 0;
					Dummy_ByteCount ++;
	
					SendMessage(NO_ERROR);
					
				} // end case Function Code: SET_COMMAND_MODE
				else if (ExtBuffer[FUNCTION_CODE] == RESET_DEVICE){
					
					// Reset_Command_Mode can be received only when wired connected to a controller
					// Mapping outbound message:
	
					// Outbound data byte count
					OutModemBuffer[Dummy_ByteCount] = 0;
					Dummy_ByteCount ++;
	
					SendMessage(NO_ERROR);
	
					// RESET
					RESET();
									
				} // end case Function Code: RESET_DEVICE
				else {
					
					Broadcasted_Inbound_Message = FALSE;		
					if((ExtBuffer[GROUP_ADDRESS]==BROADCAST) || (ExtBuffer[UNIT_ADDRESS]==MULTICAST)){
						Broadcasted_Inbound_Message = TRUE;
					}	
					Comm_Error = Execute_FC(ExtBuffer[FUNCTION_CODE], Dummy_ByteCount, Broadcasted_Inbound_Message);
					if (!(Broadcasted_Inbound_Message)){
						SendMessage(Comm_Error);
					}
				}
				
				break;   // End of COMMAND_RECEIVED
	
		}// END SystemStatus
	
	}//END Big Loop

}//END Main Program