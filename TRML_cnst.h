//	**************************************************************************************
//	
//	Project Name: Alvara� Terminal Unit
//	
//	File Name: TRML_cnst.h
//	
//	Release:	C10 - FW02 - 2.1.0
//	
//	Author:	F. Agnello
//	
//	**************************************************************************************
//	
//	� Ceravision Ltd - 2011
//	
//	*******************************************
//	*******************************************
//	* CONSTANT
//	*******************************************


/*
  FUNCTION CODE DEFINITION
*/
#define BROADCAST	0x00
#define MULTICAST	0x00
//
// READ FUNCTION CODE
//
//#define READ_RAM	0x00
#define READ_SLAVE_STATUS 0x05
#define READ_RADIO_MODEM_CHANNEL 0x06
#define READ_RADIO_MODEM_CHANNEL_EEPROM 0x07
#define GET_DEVICE_ID		0x0A
#define GET_FW_ID			0x0B
//#define GET_UNIT_ID			0x0C
//#define GET_SCENE 			0x0D
#define GET_MASTER_TIMEOUT	0x0E
//#define READ_ALL_EEPROM  0x3F
//#define READ_EEPROM 0x40
//
// WRITE FUNCTION CODE
//
#define WRITE_RAM 0x80
#define SET_RADIO_MODEM_CHANNEL 0x86
#define SET_RADIO_MODEM_CHANNEL_EEPROM 0x87
//#define GOTO_SCENE	0x88
#define SET_DEVICE_ID 		0x8A
#define SET_FW_ID			0x8B
#define SET_UNIT_ID			0x8C
//#define SET_SCENE			0x8D
#define SET_MASTER_TIMEOUT	0x8E
//#define WRITE_EEPROM 0xC0
//
// SPECIAL FUCNTION
//
#define SET_COMMAND_MODE		0xC8
//#define EXIT_COMMAND_MODE 		0xC9
#define RESET_DEVICE				0xCA

/*
* RAM ADDRESS
*/
#define RAM_POSITIONING_OFFSET 0x80

/*
*  EEPROM ADDRESS
*/
// from 0x00 to 0x0F
#define EEPROM_GROUP_ADD		0x00
#define EEPROM_UNIT_ADD			0x01
#define EEPROM_MASTER_TIMEOUT 	0x02
#define EEPROM_RF_CHANNEL		0x03
 // from 0x10 to 0x2F
#define EEPROM_SCENE_ADD		0x10
// from 0xE0 to 0xFF
#define EEPROM_DEVICE_ID 			0xE0

/*
 *  MESSAGE HEADER MAP
*/
#define FRAME_HEADER_LEN 4
#define FRAME_FOOTER_LEN 2
#define GROUP_ADDRESS 0
#define UNIT_ADDRESS 1
#define FUNCTION_CODE 2
#define BYTE_COUNT 3
#define DATA_START 4

/*
*  UNIT STATUS PARAMETERS
*/
// EEPROM

// RAM

/*
 *  FUNCTION ERROR LIST
*/
#define FUNCTION_CODE_ERROR 0xFF
#define NO_ERROR				0
#define CHECKSUM_ERROR		255
#define MD_NACK_ERROR		254
#define MD_TIMEOUT_ERROR		253
#define FC_NACK				252
#define FC_CAST_ERROR		251
#define RM_NACK_ERROR		250
#define RM_TIMEOUT_ERROR		249
//////////////////////////////////
// internal error fucntion
#define NACK_ERROR			254

/*
*  SYSTEM STATUS
*/
#define IDLE 0
#define COMMAND_RECEIVED 1
#define POWER_ON_RESET 2

/*
*  GENERAL
*/

// Boolean
#define TRUE 1
#define True 1
#define true 1
#define FALSE 0
#define False 0
#define false 0

#define EXT_BUFFER_DIM 70
#define OUT_BUFFER_DIM 70
#define MAX_ID_LEN	 31

// eeprom empty value
#define VALUE_EMPTY 255

// UNIT ID LENGTH
#define UNIT_ID_LEN 2

// Rx Idle state time out: 5msec @ 4MHz
#define RX_IDLE_TIMEOUT	5

// RadioModem Out Bound Time Out: 150msec @ 4MHz
#define RM_OUTBOUND_TIMEOUT 150

// RM_OUTBOUND_TIMEOUT online check out 
#if RX_IDLE_TIMEOUT >= RM_OUTBOUND_TIMEOUT
#error Online setting check out: RM_OUTBOUND_TIMEOUT must be higher then RX_IDLE_TIMEOUT
#endif

// Power On reset Time Out: 5sec @ 4MHz
#define POWER_ON_RESET_TIMEOUT 5000

 // Timer0 counter value: 1 msec @ 4MHz: (256 - TMRO_START_VALUE)*250usec
#define TMRO_START_VALUE 252

//COMMAND MODE SYNC TIME OUT: 2 sec @4MHz
#define CMD_MODE_TIMEOUT 2000

// Master inbound message time out: 30 sec @ 4MHz
#define MIN_MASTER_TIMEOUT 3
#define MAX_MASTER_TIMEOUT 90
#define MASTER_TIMEOUT_RELOAD 10000

// STATUS REGISTER MASK (unused bits - RB1 and RB2 are used as Rx and Tx)
//#define STATUS_MASK 0xF9
#define STATUS_MASK 0x39
#define PIR_BIT_MASK 0x09
#define PIR_A RB0
#define PIR_B RB3
								

// RADIO MODEM REGISTERS
#define AUREL_CHANNEL_REGISTER	0x02
#define AUREL_BAUDRATE_REGISTER 0x05

#define AUREL_HIGH_BAUDRATE 0x01
#define AUREL_LOW_BAUDRATE	0x00