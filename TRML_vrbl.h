//	**************************************************************************************
//	
//	Project Name: Alvara� Terminal Unit
//	
//	File Name: TRML_vrbl.h
//	
//	Release:	C10 - FW02 - 2.0.0
//	
//	Author:	F. Agnello
//	
//	**************************************************************************************
//	
//	� Ceravision Ltd - 2011
//	
//	*******************************************
//	*******************************************
//	* GLOBAL VARIABLE
//	*******************************************


// InBound Message Buffer from RF Transceiver
unsigned char ExtBuffer[EXT_BUFFER_DIM];
unsigned char ExtBufferPos=0;

// OutBound Message Buffer
unsigned char OutModemBuffer[OUT_BUFFER_DIM];

// InBound message flag
//unsigned char InBoundMessage=FALSE;
unsigned char InBoundModemMessage=0;

// System status
unsigned char SystemStatus;

// On line master status
unsigned int PowerOnReset_Sinc_Timer;
unsigned char Master_OnLine;
unsigned int Master_Inner_TimeOut_Counter;
unsigned char Master_OffLine_TimeOut_Reload;
unsigned char Master_Outer_TimeOut_Counter;

// General
unsigned char PowerOnReset;
//unsigned char RxStatus=0;
//unsigned char RxTimeOut=0;
unsigned char RxModemTimeOut=0;

unsigned char TimeOut_Timer = 0;

unsigned char RxIdleCheck=0;

unsigned char FrameLength=0;
unsigned char DataLength;

//unsigned char ModemChannel;  	
//unsigned char NewModemChannel;

unsigned char EEProm_Value;
unsigned char EEProm_Address;

//unsigned char PIR_Status;

unsigned char Broadcasted_Inbound_Message;

unsigned char Cmd_Mode_Sw;
unsigned int CmdMode_TimeOut_Timer;

// Unit adrress: GroupAddress & UnitAddress
struct UnitID
{

	unsigned char GroupAddress;
	unsigned char UnitAddress;
	
}UnitID;


union int_on_change
{
unsigned char	val;
struct
{
unsigned 		RB0 : 1;
unsigned 		RB1 : 1;
unsigned 		RB2 : 1;
unsigned 		RB3 : 1;
unsigned 		RB4 : 1;
unsigned 		RB5 : 1;
unsigned 		RB6 : 1;
unsigned 		RB7 : 1;
} line;
} int_on_change;


union portb_latch
{
unsigned char val;
struct
{
unsigned 		RB0: 1;
unsigned 		RB1: 1;
unsigned 		RB2: 1;
unsigned 		RB3: 1;
unsigned 		RB4: 1;
unsigned 		RB5: 1;
unsigned 		RB6: 1;
unsigned 		RB7: 1;
} line;
} portb_latch;


union status_latch 
{
unsigned char val;
struct 
{
unsigned 		LN0: 1;
unsigned 		LN1: 1;
unsigned 		LN2: 1;
unsigned 		LN3: 1;
unsigned 		LN4: 1;
unsigned 		LN5: 1;
unsigned 		LN6: 1;
unsigned 		LN7: 1;
} line;
} static DryContactStatus @ 0x7F;
//union status_latch 
//{
//unsigned char val;
//struct 
//{
//unsigned 		LN0: 1;
//unsigned 		LN1: 1;
//unsigned 		LN2: 1;
//unsigned 		LN3: 1;
//unsigned 		LN4: 1;
//unsigned 		LN5: 1;
//unsigned 		LN6: 1;
//unsigned 		LN7: 1;
//} line;
//} DryContactStatus;

//MOD
union modem_status
{
unsigned char val;
struct 
{
unsigned 	RF_CH: 1;
unsigned 	BPS: 1;
} stat;
} Modem_Status;

unsigned char Check_Modem;